unit ideal.logger;

interface

uses
  System.SysUtils,
  System.Classes,
  System.IOUtils,
  System.SyncObjs,
  System.Diagnostics,
  System.Types,
  System.TypInfo,
  chimera.json,
  cocinasync.collections;

type
  {$SCOPEDENUMS ON}
  TLoggingLevel = (none, fatal, errors, warnings, debug, info, trace);

  ELoggerException = class(Exception);

  IScopeCheck = interface
  end;

  TLogger = class
  strict private
    type
      TScopeCheck = class(TInterfacedObject, IScopeCheck)
      private
        FMethod : string;
        FObj : TObject;
        FSW : TStopWatch;
      public
        constructor Create(const Obj : TObject; const Method: string);
        destructor Destroy; override;
      end;
  strict private
    class var FMaxDaysHistory : integer;
    class var FConsoleCS : TCriticalSection;
    class var FAppName : string;
    class var FLogQueue : TQueue<IJSONObject>;
    class var FFilename : String;
    class var FLogThread : TThread;
    class var FLoggingLevel : TLoggingLevel;
    class var FMaxFileSize : UInt64;
    class var FStopWatch : TStopWatch;
    class var FLogEntryID : Int64;
    class procedure GenerateNewFilename;
    class procedure LogIt(jso : IJSONObject);
    class function MSToTime(ms: Int64): string; inline;
    class function F7(const str: string): string; static; inline;
  private
    class procedure DoProfileExit(obj : TObject; const MethodName : string; Duration : Int64); inline;
    class function DD(days: Int64): string; static;
    class function FF(num: Int64): string; static;
    class function FFF(num: Int64): string; static;
  protected
    class var FCleanupFinished: Boolean;
    class function DateFromFileName(sFile : string) : TDateTime;
  public
    class function Profile(MethodName : string) : IScopeCheck; overload; inline;
    class function Profile(obj : TObject; const MethodName : string) : IScopeCheck; overload; inline;

    class procedure Trace(const Msg : string; const Details : IJSONObject); overload; inline;
    class procedure Trace(const Msg : string; const Details : string = ''); overload; inline;

    class procedure Fatal(const Msg : string; const Details : IJSONObject); overload; inline;
    class procedure Fatal(const Msg : string; const Details : string = ''); overload; inline;

    class procedure Debug(const Msg : string; const Details : IJSONObject); overload; inline;
    class procedure Debug(const Msg : string; const Details : string = ''); overload; inline;

    class procedure Info(const Msg : string; const Details : IJSONObject); overload; inline;
    class procedure Info(const Msg : string; const Details : string = ''); overload; inline;

    class procedure Warning(const Msg : string; const Details : IJSONObject); overload; inline;
    class procedure Warning(const Msg : string; const Details : string = ''); overload; inline;

    class procedure License(const ID, Name : string; Expiration: TDateTime; details : IJSONObject); inline;

    class procedure Error(e: Exception; const Msg : string; const Details : IJSONObject); overload; inline;
    class procedure Error(e: Exception; const Details : IJSONObject); overload; inline;
    class procedure Error(e: Exception); overload; inline;
    class procedure Error(const Msg : string; const Details : IJSONObject = nil); overload; inline;
    class procedure Error(e: Exception; const Msg : string; const Details : string = ''); overload; inline;

    class procedure ShowLoggingLevel(ShowIfNone : boolean = false);
    class procedure CleanupHistory;

    class constructor Create;
    class destructor Destroy;

    class property LoggingLevel : TLoggingLevel read FLoggingLevel write FLoggingLevel;
    class property MaxFileSize : UInt64 read FMaxFileSize write FMaxFileSize;
    class property MaxDaysHistory : Integer read FMaxDaysHistory write FMaxDaysHistory;
    class property Filename : string read FFilename;
  end;

threadvar
    FLastMessageStamp : Int64;

implementation

uses
  {$ifdef WINDOWS}
  {$ifdef ODS}
  WinApi.Windows,
  {$endif}
  {$endif}
  System.Zip,
  System.DateUtils,
  cocinasync.async,
  ideal.utility;

{ TScopeCheck }

constructor TLogger.TScopeCheck.Create(const Obj : TObject; const Method: string);
begin
  inherited Create;
  FMethod := Method;
  FObj := Obj;
  FSW := TStopWatch.Create;
  FSW.Start;
end;

destructor TLogger.TScopeCheck.Destroy;
begin
  TLogger.DoProfileExit(FObj, FMethod, FSW.ElapsedMilliseconds);
  inherited;
end;

{ TLogger }

class procedure TLogger.GenerateNewFilename;
var
  sDT : String;
begin
  DateTimeToString(sDT, 'yyyy-mm-dd hh.nn.ss.zzz', Now);
  FFilename :=
    TPath.GetHomePath+TPath.DirectorySeparatorChar+
    'ideal'+TPath.DirectorySeparatorChar+
    'logs'+TPath.DirectorySeparatorChar+
    FAppName+TPath.DirectorySeparatorChar+
    FAppName+'.'+sDT+'.json';
  TDirectory.CreateDirectory(ExtractFilePath(FFilename));
  TInterlocked.Exchange(FLogEntryID, 0);
  {$ifdef WINDOWS}
  {$ifdef ODS}
  OutputDebugString(PChar('log file '+FFilename));
  {$endif}
  {$endif}
end;

class procedure TLogger.ShowLoggingLevel(ShowIfNone : boolean = false);
var
  jso : IJSONObject;
  sLoggingLevel : string;
begin
  if (FLoggingLevel = TLoggingLevel.none) and (ShowIfNone = false) then
    exit;
  sLoggingLevel :=  (GetEnumName(TypeInfo(TLoggingLevel), ord(FLoggingLevel))).ToUpper();
  jso := TJSON.New;
  jso.Strings['@l'] := '';
  jso.Strings['@m'] := 'Logging Level: '+sLoggingLevel;
  jso.Objects['@detail'] := TJSON.New;
    jso.Objects['@detail'] := TJSON.New;
  LogIt(jso);
end;

class function TLogger.DateFromFileName(sFile : string) : TDateTime;
var
  ary : TArray<string>;
  str : string;
begin
  str := sFile.Substring(0,sFile.Length-ExtractFileExt(sFile).Length);
  str := str.Substring(length(str)-23);
  Result := Now;
  ary := str.Split([' ']);
  if (length(ary) = 2) and (length(ary[0]) = 10) and (length(ary[1]) = 12) then
  begin
    try
      Result :=
        EncodeDate(ary[0].Substring(0,4).ToInteger, ary[0].Substring(5,2).ToInteger, ary[0].Substring(8,2).ToInteger)+
        EncodeTime(ary[1].Substring(0,2).ToInteger, ary[1].Substring(3,2).ToInteger, ary[1].Substring(6,2).ToInteger, ary[1].Substring(9,3).ToInteger);
    except
    end;
  end;
end;

class procedure TLogger.CleanupHistory;
var
  sPath : string;
  ary : TStringDynArray;
  sFile : String;
  dt : TDateTime;
  zip : TZipFile;
begin
  try
    Info('TLogger.CleanupHistory started');
    sPath := ExtractFilePath(FFilename);
    ary := TDirectory.GetFiles(sPath,FAppName+'.*.json');
    for sFile in ary do
    begin
      if sFile = FFilename then
        Continue;
      zip := TZipFile.Create;
      try
        zip.Open(sFile+'z',TZipMode.zmWrite);
        zip.Add(sFile);
        zip.Close;
        TFile.Delete(sFile);
      finally
        zip.Free;
      end;
    end;

    ary := TDirectory.GetFiles(sPath,FAppName+'.*.jsonz');
    for sFile in ary do
    begin
      if sFile = FFilename then
        Continue;
      dt := DateFromFilename(sFile);
      if DaysBetween(dt, Now) > FMaxDaysHistory then
      begin
        TFile.Delete(sFile);
      end;
    end;
  finally
    FCleanupFinished := True;
    Info('TLogger.CleanupHistory finished');
  end;
end;

class constructor TLogger.Create;

  function CreateLoggerThread : TThread;
  begin
    Result := TThread.CreateAnonymousThread(
      procedure
        procedure EmptyQueue;
        var
          jso : IJSONObject;
          fs : TFileStream;
          ary : TArray<Byte>;
        begin
          fs := nil;
          try
            repeat
              jso := FLogQueue.Dequeue;
              if Assigned(jso) then
              begin
                if not Assigned(fs) then
                begin
                  if TFile.Exists(FFilename) then
                  begin
                    fs := TFileStream.Create(FFilename, fmOpenReadWrite or fmShareDenyWrite);
                    fs.Position := fs.Size;
                  end else
                    fs := TFileStream.Create(FFilename, fmCreate or fmShareDenyWrite);
                end else
                  fs.Position := fs.Size;
                if fs.Size > FMaxFileSize then
                begin
                  FreeAndNil(fs);
                  GenerateNewFilename;
                  Continue;
                end;
                //jso.Dates['log_stamp'] := Now;
                ary := TEncoding.UTF8.GetBytes(jso.AsJSON(TWhitespace.compact)+sLineBreak);
                fs.Write(ary, Length(ary));
              end;
            until not assigned(jso);
          finally
            fs.Free;  // free checks for nil
          end;
        end;
      begin
        while not TThread.CheckTerminated do
        begin
          EmptyQueue;
          sleep(100);
        end;
        EmptyQueue;
        FLogThread := nil;
      end
    );
    Result.Start;
  end;
begin
  FMaxDaysHistory := 365;
  FConsoleCS := TCriticalSection.Create;
  FLoggingLevel := TLoggingLevel.warnings; // Default level.
  FAppName := ExtractFileName(ModuleFileName);
  FAppName := FAppName.Replace(ExtractFileExt(FAppName),'', [rfReplaceAll]);
  GenerateNewFilename;
  FLogQueue := TQueue<IJSONObject>.Create(1024*10);
  FMaxFileSize := 262144000; // 250MB
  FStopWatch := TStopWatch.Create;
  FStopWatch.Start;

  ShowLoggingLevel(False);
  Info('Application Startup');

  FLogThread := CreateloggerThread;

  Async.AfterDo(10000,
    procedure
    begin
      CleanupHistory;
    end,
    False
  );
end;

class procedure TLogger.Debug(const Msg, Details: string);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.debug then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'DEBUG';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := TJSON.New;
  if Details <> '' then
    jso.Objects['@detail'].Strings['text'] := Details;
  LogIt(jso);
end;

class procedure TLogger.Debug(const Msg: string; const Details: IJSONObject);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.debug then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'DEBUG';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := Details.Clone;
  LogIt(jso);
end;

class destructor TLogger.Destroy;
begin
  Info('Application Shutdown');
  FLogThread.Terminate;
  while Assigned(FLogThread) do
    sleep(100);
  FConsoleCS.Free;
end;

class procedure TLogger.DoProfileExit(obj: TObject; const MethodName: string; Duration : Int64);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.info then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'EXIT';
  jso.Strings['@m'] := MethodName;
  jso.Integers['@method_duration'] := Duration;
  jso.Objects['@detail'] := TJSON.New;
  LogIt(jso);
end;

class procedure TLogger.Error(const Msg: string; const Details: IJSONObject);
begin
  if FLoggingLevel < TLoggingLevel.errors then
    exit;
  try
    raise ELoggerException.Create(Msg);
  except
    on e: ELoggerException do
      Error(e, Msg, Details);
  end;
end;

class procedure TLogger.Error(e: Exception; const Msg: string;
  const Details: IJSONObject);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.errors then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'ERROR';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := Details;
  jso.Objects['@x'] := TJSON.New;
  if Assigned(e) then
  begin
    jso.Objects['@x'].Strings['class'] := e.ClassName;
    jso.Objects['@x'].Strings['message'] := e.Message;
    jso.Objects['@x'].Strings['call_stack'] := e.StackTrace;
  end;
  LogIt(jso);
end;

class procedure TLogger.Error(e: Exception; const Msg, Details: string);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.errors then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'ERROR';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := TJSON.New;
  if Details <> '' then
    jso.Objects['@detail'].Strings['text'] := Details;
  jso.Objects['@x'] := TJSON.New;
  if Assigned(e) then
  begin
    jso.Objects['@x'].Strings['class'] := e.ClassName;
    jso.Objects['@x'].Strings['message'] := e.Message;
    jso.Objects['@x'].Strings['call_stack'] := e.StackTrace;
  end;
  LogIt(jso);
end;

class procedure TLogger.Fatal(const Msg, Details: string);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.Fatal then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'FATAL';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := TJSON.New;
  if Details <> '' then
    jso.Objects['@detail'].Strings['text'] := Details;
  LogIt(jso);
end;

class procedure TLogger.Fatal(const Msg: string; const Details: IJSONObject);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.Fatal then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'FATAL';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := Details.Clone;
  LogIt(jso);
end;

class procedure TLogger.Error(e: Exception; const Details: IJSONObject);
begin
  if FLoggingLevel < TLoggingLevel.errors then
    exit;
  Error(e, '', Details);
end;

class procedure TLogger.Error(e: Exception);
var
  jso : IJSONObject;
begin
  if (FLoggingLevel < TLoggingLevel.errors) or not Assigned(e) then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'ERROR';
  jso.Strings['@m'] := e.Message;
  jso.Objects['@detail'] := TJSON.New;
  jso.Objects['@x'] := TJSON.New;
  jso.Objects['@x'].Strings['class'] := e.ClassName;
  jso.Objects['@x'].Strings['message'] := e.Message;
  jso.Objects['@x'].Strings['call_stack'] := e.StackTrace;
  LogIt(jso);
end;

class procedure TLogger.Info(const Msg, Details: string);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.info then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'INFO';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := TJSON.New;
  if Details <> '' then
    jso.Objects['@detail'].Strings['text'] := Details
  else
    jso.Objects['@detail'] := TJSON.New;
  LogIt(jso);
end;

class procedure TLogger.Info(const Msg: string; const Details: IJSONObject);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.info then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'INFO';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := Details.Clone;
  LogIt(jso);
end;

class function TLogger.DD(days : Int64) : string;
begin
  Result := days.ToString;
  while Result.Length < 3 do
    Result := ' '+Result;
end;

class function TLogger.FF(num : Int64) : string;
begin
  Result := num.ToString;
  while Result.Length < 2 do
    Result := '0'+Result;
end;

class function TLogger.FFF(num : Int64) : string;
begin
  Result := num.ToString;
  while Result.Length < 3 do
    Result := '0'+Result;
end;

class function TLogger.MSToTime(ms : Int64) : string;
var
  iDays, iHours, iMins, iSecs, iMS : int64;
begin
  iDays := Trunc(ms / 1000 / 60 / 60 / 24);
  ms := ms - (iDays * 24 * 60 * 60 * 1000);
  iHours := Trunc(ms / 1000 / 60 / 60);
  ms := ms - (iHours * 60 * 60 * 1000);
  iMins := Trunc(ms / 1000 / 60);
  ms := ms - (iMins * 60 * 1000);
  iSecs := Trunc(ms / 1000);
  ms := ms - (iSecs * 1000);
  iMs := ms;
  Result := FF(iSecs)+'.'+FFF(iMS);
  if iMins + iHours + iDays > 0 then
    Result := FF(iMins)+':'+Result;
  if iHours + iDays > 0 then
    Result := FF(iHours)+':'+Result;
  if iDays > 0 then
    Result := DD(iDays)+':'+Result;
end;

class function TLogger.F7(const str : string) : string;
begin
  Result := str;
  while Result.Length < 7 do
    Result := Result+' ';
end;

class procedure TLogger.License(const ID, Name : string; Expiration: TDateTime; Details : IJSONObject);
var
  jso, jsoDetail : IJSONObject;
begin
  jso := TJSON.New;
  jso.Strings['@l'] := 'LICENSE';
  jso.Strings['@m'] := ID+' '+Name+' expires '+DateToStr(Expiration);
  jsoDetail := Details.clone;
  jsoDetail.Strings['id'] := ID;
  jsoDetail.Strings['name'] := Name;
  jsoDetail.Dates['expiration'] := Expiration;
  jso.Objects['@detail'] := jsoDetail;
  LogIt(jso);
end;

class procedure TLogger.LogIt(jso: IJSONObject);
var
  sMsg, sStamp : string;
  iStamp : Int64;
  x : IJSONObject;
begin
  iStamp := FStopWatch.ElapsedMilliseconds;
  jso.Integers['@i'] := TInterlocked.Increment(FLogEntryID);
  jso.Integers['@thread_id'] := TThread.Current.ThreadID;
  jso.Booleans['@is_main_thread'] := TThread.Current.ThreadID = MainThreadID;
  jso.Dates['@t'] := Now;
  jso.Integers['@ms_from_launch'] := iStamp;
  jso.Integers['@ms_delta'] := iStamp - FLastMessageStamp;
  jso.Strings['@offset'] := MSToTime(jso.Integers['@ms_from_launch']);
  jso.Strings['@delta'] := MSToTime(jso.Integers['@ms_delta']);
  if not jso.Has['@method_duration'] then
    jso.AddNull('@method_duration');
  if not jso.Has['@x'] then
    jso.AddNull('@x');
  if not jso.Has['@detail'] then
    jso.AddNull('@detail');

  FLastMessageStamp := iStamp;
  jso.strings['@app_name'] := FAppName;
  FLogQueue.Enqueue(jso);
  if IsConsole then
  begin
    DateTimeToString(sStamp, 'yyyy-mm-dd hh:nn:ss.zzz', jso.Dates['@t']);
    sMsg := sStamp+'  '+jso.Strings['@offset']+'  '+jso.Strings['@delta']+'  '+F7(jso.Strings['@l'])+'  ';
    FConsoleCS.Enter;
    try
      if jso.Strings['@l'] = 'PROFILE' then
        WriteLn(sMsg+jso.StringsDefaulted['@m'] +' '+jso.IntegersDefaulted['@duration'].ToString)
      else if jso.Strings['@l'] = 'ERROR' then
      begin
        x := jso.Objects['@x'];
        if x.Strings['message'] <> jso.Strings['@m'] then
          WriteLn(sMsg + x.StringsDefaulted['class'] + ' ' + x.StringsDefaulted['message']  +' : ' + jso.StringsDefaulted['@m'])
        else
          WriteLn(sMsg + x.StringsDefaulted['class'] + ' ' + x.StringsDefaulted['message']);
        if x.StringsDefaulted['call_stack'] <> '' then
          WriteLn(x.StringsDefaulted['call_stack']);
      end else
        WriteLn(sMsg+jso.StringsDefaulted['@m']);
    finally
      FConsoleCS.Leave;
    end;
  end;
  {$ifdef ODS}
    try
      sMsg := jso.Strings['@m'];
      OutputDebugString(PChar(sMsg));
    except
    end;
  {$endif}

end;

class function TLogger.Profile(obj: TObject; const MethodName: string): IScopeCheck;
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.info then
    exit(nil);
  jso := TJSON.New;
  jso.Strings['@l'] := 'ENTER';
  jso.Strings['@m'] := MethodName;
  jso.Objects['@detail'] := TJSON.New;
  LogIt(jso);
  Result := TScopeCheck.Create(obj, MethodName);
end;

class function TLogger.Profile(MethodName: string): IScopeCheck;
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.info then
    exit(nil);
  jso := TJSON.New;
  jso.Strings['@l'] := 'ENTER';
  jso.Strings['@m'] := MethodName;
  jso.Objects['@detail'] := TJSON.New;
  LogIt(jso);
  Result := TScopeCheck.Create(nil, MethodName);
end;

class procedure TLogger.Trace(const Msg: string; const Details: IJSONObject);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.trace then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'TRACE';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := Details.Clone;
  LogIt(jso);
end;

class procedure TLogger.Trace(const Msg, Details: string);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.trace then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'TRACE';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := TJSON.New;
  if Details <> '' then
    jso.Objects['@detail'].Strings['text'] := Details;
  LogIt(jso);
end;

class procedure TLogger.Warning(const Msg: string; const Details: IJSONObject);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.warnings then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'WARN';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := Details.Clone;
  LogIt(jso);
end;

class procedure TLogger.Warning(const Msg, Details: string);
var
  jso : IJSONObject;
begin
  if FLoggingLevel < TLoggingLevel.warnings then
    exit;
  jso := TJSON.New;
  jso.Strings['@l'] := 'WARN';
  jso.Strings['@m'] := Msg;
  jso.Objects['@detail'] := TJSON.New;
  if Details <> '' then
    jso.Objects['@detail'].Strings['text'] := Details;
  LogIt(jso);
end;

end.
